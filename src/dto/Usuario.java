package dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Usuario {
	@Id
	private String cedula;
	private String apellido;

	private String nombre;
	private String email;
	private String telefono; 
	private String username;
	private String password;

	@OneToMany(mappedBy = "usuario", cascade = javax.persistence.CascadeType.ALL )	
	private Set<Cita> citas = new HashSet();

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spa")
	private Spa spa;
	
	public Usuario(Spa spa, String cedula, String apellido, String nombre, String email, String telefono) {
		super();
		this.spa=spa;
		this.cedula = cedula;
		this.apellido = apellido;
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
	}
	public Usuario()
	{
		
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (citas == null) {
			if (other.citas != null)
				return false;
		} else if (!citas.equals(other.citas))
			return false;
		return true;
	}
	
	
	
	public String getApellido() {
		return apellido;
	}

	public String getCedula() {
		return cedula;
	}

	public Set<Cita> getCitas() {
		return citas;
	}

	public String getEmail() {
		return email;
	}

	public String getNombre() {
		return nombre;
	}

	public String getPassword() {
		return password;
	}

	public Spa getSpa() {
		return spa;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((citas == null) ? 0 : citas.hashCode());
		return result;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public void setCitas(Set<Cita> citas) {
		this.citas = citas;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSpa(Spa spa) {
		this.spa = spa;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
