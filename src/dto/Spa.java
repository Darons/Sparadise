package dto;

import java.util.List;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Spa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigo;

	@OneToMany(mappedBy = "spa", cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Usuario> usuarios = new HashSet();
	// done
	@OneToMany(mappedBy = "spa", cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Cita> citas = new HashSet();
	// done
	@OneToMany(mappedBy = "spa", cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Servicio> servicios = new HashSet();
	// done
	@OneToMany(mappedBy = "spa", cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Superusuario> superusuarios = new HashSet();
	// done
	@OneToMany(mappedBy = "spa", cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Plan> planes = new HashSet();
	// done
	private String descripcion, nombre, pais, ciudad, direccion, telefono, web, email, facebook, twitter, instagram,
			imagen;

	public Spa() {
	}

	public Spa(long codigo, String descripcion, String nombre, String pais, String ciudad, String direccion,
			String telefono, String web, String email, String facebook, String twitter, String instagram,
			String imagen) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.nombre = nombre;
		this.pais = pais;
		this.ciudad = ciudad;
		this.direccion = direccion;
		this.telefono = telefono;
		this.web = web;
		this.email = email;
		this.facebook = facebook;
		this.twitter = twitter;
		this.instagram = instagram;
		this.imagen = imagen;
	}

	public Spa(String descripcion, String nombre, String pais, String ciudad, String direccion, String telefono,
			String web, String email, String facebook, String twitter, String instagram, String imagen) {
		super();
		this.descripcion = descripcion;
		this.nombre = nombre;
		this.pais = pais;
		this.ciudad = ciudad;
		this.direccion = direccion;
		this.telefono = telefono;
		this.web = web;
		this.email = email;
		this.facebook = facebook;
		this.twitter = twitter;
		this.instagram = instagram;
		this.imagen = imagen;
	}

	public Set<Cita> getCitas() {
		return citas;
	}

	public String getCiudad() {
		return ciudad;
	}

	public long getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getEmail() {
		return email;
	}

	public String getFacebook() {
		return facebook;
	}

	public String getImagen() {
		return imagen;
	}

	public String getInstagram() {
		return instagram;
	}

	public String getNombre() {
		return nombre;
	}

	public String getPais() {
		return pais;
	}

	public Set<Plan> getPlanes() {
		return planes;
	}

	public Set<Servicio> getServicios() {
		return servicios;
	}

	public Set<Superusuario> getSuperusuarios() {
		return superusuarios;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getTwitter() {
		return twitter;
	}

	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public String getWeb() {
		return web;
	}

	public void setCitas(Set<Cita> citas) {
		this.citas = citas;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public void setPlanes(Set<Plan> planes) {
		this.planes = planes;
	}

	public void setServicios(Set<Servicio> servicios) {
		this.servicios = servicios;
	}

	public void setSuperusuarios(Set<Superusuario> superusuarios) {
		this.superusuarios = superusuarios;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public void setWeb(String web) {
		this.web = web;
	}
}