package dto;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Superusuario {

	@Id
	private String id;
	private String password;

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spa")
	private Spa spa;
	
	
	public Superusuario() {
		super();
	}

	public Superusuario(Spa spa,String id, String password) {
		super();
		this.spa=spa;
		this.id = id;
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public Spa getSpa() {
		return spa;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSpa(Spa spa) {
		this.spa = spa;
	}
	
	
	
	
}
