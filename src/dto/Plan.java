package dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Plan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigo;

	private String descripcion;

	private double costo;

	@ManyToMany(cascade = { javax.persistence.CascadeType.MERGE,
			javax.persistence.CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "plan_servicio", inverseJoinColumns = { @JoinColumn(name = "servicio") }, joinColumns = {
			@JoinColumn(name = "plan") })
	private Set<Servicio> servicios = new HashSet();

	@OneToMany(cascade=javax.persistence.CascadeType.ALL ,mappedBy = "plan", fetch=FetchType.LAZY)
	private Set<Cita> citas = new HashSet();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spa", nullable = false)
	private Spa spa;


	
	public Plan() {
		super();
	}
	
	
	public Plan(Spa spa,String descripcion, double costo) {
		super();
		this.spa=spa;
		this.descripcion = descripcion;
		this.costo = costo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plan other = (Plan) obj;
		if (citas == null) {
			if (other.citas != null)
				return false;
		} else if (!citas.equals(other.citas))
			return false;
		if (servicios == null) {
			if (other.servicios != null)
				return false;
		} else if (!servicios.equals(other.servicios))
			return false;
		return true;
	}

	public Set<Cita> getCitas() {
		return citas;
	}

	public long getCodigo() {
		return codigo;
	}

	public double getCosto() {
		return costo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Set<Servicio> getServicios() {
		return servicios;
	}

	public Spa getSpa() {
		return spa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((citas == null) ? 0 : citas.hashCode());
		result = prime * result + ((servicios == null) ? 0 : servicios.hashCode());
		return result;
	}

	public void setCitas(Set<Cita> citas) {
		this.citas = citas;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setServicios(Set<Servicio> servicios) {
		this.servicios = servicios;
	}

	public void setSpa(Spa spa) {
		this.spa = spa;
	}

	
	
	
}
