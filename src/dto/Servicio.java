package dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Servicio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigo;

	private String descripcion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spa", nullable = false)
	private Spa spa;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "servicios", cascade = CascadeType.ALL)
	private Set<Plan> planes = new HashSet();

	public Servicio(Spa spa, String descripcion) {
		this.spa=spa;
		this.descripcion=descripcion;
	}

	public Servicio() {	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servicio other = (Servicio) obj;
		if (planes == null) {
			if (other.planes != null)
				return false;
		} else if (!planes.equals(other.planes))
			return false;
		return true;
	}

	public long getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Set<Plan> getPlanes() {
		return planes;
	}

	public Spa getSpa() {
		return spa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planes == null) ? 0 : planes.hashCode());
		return result;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setPlanes(Set<Plan> planes) {
		this.planes = planes;
	}

	public void setSpa(Spa spa) {
		this.spa = spa;
	}

}
