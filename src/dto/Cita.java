package dto;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Cita {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "plan", nullable = false)
	private Plan plan;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usuario", nullable = false)
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "spa", nullable = false)
	private Spa spa;

	@Basic
	@Temporal(TemporalType.DATE)
	private Date fecha; //ESTO ES YYYY-MM-DD
	
	
	public Cita(Spa spa,Plan plan, Usuario usuario, Date fecha) {
		super();
		this.spa=spa;
		this.plan = plan;
		this.usuario = usuario;
		this.fecha = fecha;
	}
	
	public Cita() {}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cita other = (Cita) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}

	public long getCodigo() {
		return codigo;
	}

	public Date getFecha() {
		return fecha;
	}

	public Plan getPlan() {
		return plan;
	}

	public Spa getSpa() {
		return spa;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (codigo ^ (codigo >>> 32));
		return result;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public void setSpa(Spa spa) {
		this.spa = spa;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
