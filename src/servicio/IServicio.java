package servicio;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import dao.IHibernateDAO;
import dto.Cita;
import dto.Plan;
import dto.Servicio;
import dto.Superusuario;
import dto.Usuario;
import dto.Spa;

public abstract interface IServicio {

	public void guardarUsuario(Usuario usuario);

	public void guardarAdmin(Superusuario superusuario);

	public void guardarServicio(Servicio servicio);

	public void guardarCita(Cita cita);

	public void guardarPlan(Plan plan);

	public void setHibernateDAO(IHibernateDAO hibernateDAO);

	public Usuario buscarUsuario(String cedula);

	public Superusuario buscarSuperusuario(String id);

	public Servicio buscarServicio(long codigo);

	public Cita buscarCita(long codigo);

	public Plan buscarPlan( long codigo);

	public List buscarTodosLosClientes(Spa spa);

	public List buscarTodosLosServicios(Spa spa);

	public List buscarTodosLosPlanes(Spa spa);

	public void eliminarUsuario(Usuario usuario);

	public void eliminarServicio(Servicio servicio);

	public void eliminarCita(Cita cita);

	public void eliminarPlan(Plan plan);

	public boolean verificarDisponibilidad(Spa spa, Date fecha);

	public double calcularPagoParcial(Plan plan);

	public void guardarSpa(Spa spa);

}
