package servicio.impl;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.IHibernateDAO;
import dto.Cita;
import dto.Plan;
import dto.Servicio;
import dto.Superusuario;
import dto.Usuario;
import servicio.IServicio;
import dto.Spa;

public class Servicios implements IServicio {

	IHibernateDAO hibernateDAO;

	@Override
	public Cita buscarCita(long codigo) {
		
		
		return (Cita) hibernateDAO.findByKey(Cita.class,codigo);
	}

	@Override
	public Plan buscarPlan(long codigo) {
		return (Plan) hibernateDAO.findByKey(Plan.class, codigo);
	}

	@Override
	public Servicio buscarServicio(long codigo) {
		return (Servicio) hibernateDAO.findByKey(Servicio.class, codigo);
	}

	@Override
	public Superusuario buscarSuperusuario( String id) {
		return (Superusuario) hibernateDAO.findByKey(Superusuario.class, id);
	}

	@Override
	public List buscarTodosLosClientes(Spa spa) {
		return hibernateDAO.loadAllSpa(spa, Usuario.class);
	}

	@Override
	public List buscarTodosLosPlanes(Spa spa) {
		return hibernateDAO.loadAllSpa(spa, Plan.class);
	}

	@Override
	public List buscarTodosLosServicios(Spa spa) {
		return hibernateDAO.loadAllSpa(spa, Servicio.class);
	}

	@Override
	public Usuario buscarUsuario(String cedula) {
		return (Usuario) hibernateDAO.findByKey(Usuario.class, cedula);
	}

	@Override
	public double calcularPagoParcial(Plan plan) {
		return plan.getCosto() * 0.2;
	}

	@Override
	public void eliminarCita(Cita cita) {
		hibernateDAO.delete(cita);
	}

	@Override
	public void eliminarPlan(Plan plan) {
		hibernateDAO.delete(plan);
	}

	@Override
	public void eliminarServicio(Servicio servicio) {
		hibernateDAO.delete(servicio);
	}

	public void eliminarSpa(Spa spa)
	{
		hibernateDAO.delete(spa);
	}

	@Override
	public void eliminarUsuario(Usuario usuario) {
		hibernateDAO.delete(usuario);
	}

	public IHibernateDAO getHibernateDAO() {
		return hibernateDAO;
	}

	@Override
	public void guardarAdmin(Superusuario superusuario){
		
		if(superusuario.getSpa()!= null)
		{
			Spa spa= superusuario.getSpa();
			ArrayList admin = new ArrayList();
			admin = (ArrayList) hibernateDAO.loadAllSpa(spa, Superusuario.class);
	
			if (admin.isEmpty()) {
				if (superusuario.getPassword() != null && superusuario.getId() != null) {
	
					hibernateDAO.saveOrUpdate(superusuario);
				} else {
					System.out.println("Debe llenar los campos de identidad y contraseña");
				}
			} else
				System.out.println("Solo se puede registrar a un administrador");
		} else{
			System.out.println("Un administrador necesita un spa al que administrar.");
		}
			
	}

	@Override
	public void guardarCita(Cita cita){
		
			
			if (cita.getSpa() != null) {
				Spa spa= cita.getSpa();
				ArrayList planes = new ArrayList();
				planes = (ArrayList) hibernateDAO.loadAllSpa(spa, Plan.class);
				if (!planes.isEmpty()) {
					if (cita.getFecha() != null && cita.getPlan() != null && cita.getUsuario() != null
							&& verificarDisponibilidad(spa, cita.getFecha())) {
						hibernateDAO.saveOrUpdate(cita);
					} else {
						System.out.println(
								"Debe llenar los campos de fecha, plan selecionado y del usuario para procesar la reservación");
					}
				} else
					System.out.println("No puede registrar una cita ya que no hay planes registrados");
			}else
			{
				System.out.println("La cita debe ser asignada a un spa");
			}
	}

	@Override
	public void guardarPlan(Plan plan){
		if (plan.getSpa() != null) {
			if (!plan.getServicios().isEmpty()) {
				if (plan.getDescripcion() != null && plan.getCosto() >= 0) {
					hibernateDAO.saveOrUpdate(plan);
				} else {
					System.out.println("Debe llenar los campos de descripcion y asignar un costo positivo");
				}
			} else
				System.out.println("No puede registrar un plan ya que no hay servicios registrados en el mismo");
		} else 
			System.out.println("El plan debe pertenecer a un spa");
	}

	@Override
	public void guardarServicio(Servicio servicio){
			if (servicio.getSpa() != null) {
				if (servicio.getDescripcion() != null) {
					hibernateDAO.saveOrUpdate(servicio);
				} else {
					System.out.println("Debe llenar los campos de codigo y descripción");
				} 
			} else
			{
				System.out.println("El servicio debe pertenecer a un spa");
			}
	}
	
	@Override
	public void guardarSpa(Spa spa) {
		
		
		if(spa.getCiudad() != null && spa.getNombre() != null && spa.getPais() != null &&
				spa.getDescripcion() != null && spa.getTelefono() != null && spa.getEmail() != null && spa.getDireccion() != null)
			hibernateDAO.saveOrUpdate(spa);
		else {
			System.out.println("El spa debe tener nombre, descripcion, pais, ciudad, direccion, telefono y email");
		}
	}

	@Override
	public void guardarUsuario(Usuario usuario){
		if (usuario.getCedula() != null && usuario.getNombre() != null && usuario.getApellido() != null && usuario.getSpa() != null) {
			
			hibernateDAO.saveOrUpdate(usuario);
		} else {
			System.out.println("Debe llenar los campos de cedula, nombre, apellido, nombre de usuario y contrase�a, adem�s del spa en al que pertenece");
		}
	}

	@Override
	public void setHibernateDAO(IHibernateDAO hibernateDAO) {
		this.hibernateDAO = hibernateDAO;
	}

	@Override
	public boolean verificarDisponibilidad(Spa spa, Date fecha) {
		


		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		String stringFecha = formatter.format(fecha);


		hibernateDAO.getCurrentSession().beginTransaction();
		String codigo = "SELECT *  FROM Cita where fecha='" + stringFecha + "';";
		ArrayList disp = new ArrayList();
		disp = (ArrayList) hibernateDAO.findBySQLQuery(codigo);		
		hibernateDAO.getCurrentSession().getTransaction().commit();
		if (disp.size() >= 2) {
			return false;
		} else
			return true;
	}

	
}
